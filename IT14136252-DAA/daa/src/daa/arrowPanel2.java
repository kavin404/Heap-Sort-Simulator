package daa;

import java.awt.*;
import javax.swing.*;

public class arrowPanel2 extends JPanel
{
    private int gap;
    private int h;
    private int num;
    private int start;
    private int tot;
    private int offset;

    public arrowPanel2()
    {
        super.setBackground(Color.yellow);     
    }
        @Override
    public Dimension getPreferredSize()
    {
      
        return new Dimension(0,0);
    }
    public void setXY(int gap,int num,int h,int start,int totalArraySize,int offset){
        
        this.gap=gap;
        this.num=num;
        this.start=start;
        this.tot=totalArraySize;
        this.offset=offset/2-1;
       
    }
    
    @Override
    public void paintComponent(Graphics g)
    {

            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setStroke(new BasicStroke(3));
            int rootGap=1;
            int childGap=1;
            for(int i=1;i<num;i++){

                g.drawLine(gap*rootGap-25,h*60,(start*(childGap))-25,h*60+60);
                childGap+=2;
                g.drawLine(gap*rootGap+25,h*60,(start*(childGap))+25,h*60+60);
                rootGap+=2;
                childGap+=2;
               
            }
    }


   
}