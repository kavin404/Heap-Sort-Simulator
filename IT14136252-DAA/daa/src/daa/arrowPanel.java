package daa;

import java.awt.*;
import javax.swing.*;

public class arrowPanel extends JPanel
{
   
    private int x;
    private int y;
    private int ax[] = new int[3];
    private int ay[]= new int[3];

    public arrowPanel()
    {
        super.setBackground(Color.white);

    }
    public void setXY(int x,int y){
        this.x=x;
        this.y=y;
      
    }

    @Override
    public void paintComponent(Graphics g)
    {
            ax[0]=x-5;
            ax[1]=x+5;
            ax[2]=x;
            ay[0]=10;
            ay[1]=10;
            ay[2]=4;
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setStroke(new BasicStroke(5));
            g.drawPolygon(ax,ay,3);
            g.drawLine(x,4,x,20);
            g.drawLine(150,20,x,20);
            g.drawLine(150,20,150,40);
            

    }

    

   
}