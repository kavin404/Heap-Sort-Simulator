/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daa;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

/**
 *
 * @author kavindu
 */
public class InsertionSort extends javax.swing.JFrame{
    int arr[];
    JLabel a[];
    JLabel index[];
    JLabel key= new JLabel("j");
    JLabel ii= new JLabel("i");
    int delay=500;
    arrowPanel panel;
    public InsertionSort() {
    
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int width=dim.width/2-(this.getWidth()/2);
        int height=  dim.height/2-(this.getHeight()/2);
        setBounds(width,height,this.getWidth(),this.getHeight());
      
        panel = new arrowPanel();
        panel.setBounds(0,170,1000,50);
        panel.setVisible(false);
        jPanel2.add(panel);
        setCord();       
        File file = new File((getClass().getResource("../arrow.png")).getFile());      
        lbl_arrow.setIcon(new ImageIcon(new ImageIcon(file.getAbsolutePath()).getImage().getScaledInstance(80, 40,java.awt.Image.SCALE_SMOOTH)));
        lbl_arrow.setVisible(false);
       
    }
    
    public void setCord(){
        
        DefaultListModel listModel = new DefaultListModel();
        listModel.addElement("for ( int j = 1; j<arr.length; j++ ){");
        listModel.addElement("      int key = arr[ j ];");
        listModel.addElement("      int i = j-1;");
        listModel.addElement("      while ( i>=0 && arr[ i ]>key ){");
        listModel.addElement("          arr[ i+1 ] = arr[ i ];");
        listModel.addElement("          i--;}");
        listModel.addElement("      arr[ i+1 ] = key;}");
   
       
        jList1.setModel(listModel);
        jList1.setFixedCellHeight(40);
  
        
    }
     Thread M;
    public void insertionSort(){
        lbl_arrow.setVisible(false);
        M = new Thread(){
            public void run(){
             for(int j=1;j<arr.length;j++){
                  jList1.setSelectedIndex(0);
                   try {
                    sleep(delay);
                
                    key.setBounds(60+(j*50),30,30,30);
                    key.setVisible(true);
                    int key=arr[j];
                    
                    jList1.setSelectedIndex(1);
                    sleep(delay);
                    lbl_key.setText(Integer.toString(key));
                    lbl_keyIndex.setText(Integer.toString(j));
                    jList1.setSelectedIndex(2);
                    sleep(delay);  
                    int i=j-1;
                    lbl_iIndex.setText(Integer.toString(i));
                    ii.setBounds(60+(i*50),30,30,30);
                    ii.setVisible(true);
                    sleep(delay/2);
                    jList1.setSelectedIndex(3);
                    sleep(delay);
                    lbl_con.setText(i+" > = 0   &&  "+arr[i]+" > "+key);
                    boolean check=false;
                    while(i>=0 && arr[i]>key){
                        jList1.setSelectedIndex(3);
                        check=true;
                        lbl_con.setForeground(Color.green);
                        sleep(delay);
                        jList1.setCellRenderer(new DefaultListCellRenderer() {
                           @Override
                           public Component getListCellRendererComponent(JList list, Object value, int index,
                                      boolean isSelected, boolean cellHasFocus) {
                               Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                               if(isSelected && list.getSelectedIndex()==3){

                                       setBackground(Color.GREEN);

                               }
                               return c;
                           }

                       });

                           try {

                               sleep(delay);
                               arr[i+1]=arr[i];
                               jList1.setSelectedIndex(4);
                               sleep(delay);
                               lbl_arrow.setBounds(60+(i*50),150,100,30);
                               lbl_arrow.setVisible(true);
                               a[i+1].setBackground(Color.red);
                               a[i].setBackground(Color.green);
                               sleep(delay);
                               a[i+1].setText(a[i].getText());
                               a[i+1].setBackground(Color.green);
                               sleep(delay);
                               lbl_arrow.setVisible(false);
                               a[i+1].setBackground(Color.white);
                               a[i].setBackground(Color.white);
                               i--;
                               jList1.setSelectedIndex(5);
                               sleep(delay);
                               lbl_iIndex.setText(Integer.toString(i));
                               ii.setBounds(60+(i*50),30,30,30);
                               sleep(delay/2);
                               ii.setVisible(true);
                           } catch (InterruptedException ex) {
                               Logger.getLogger(InsertionSort.class.getName()).log(Level.SEVERE, null, ex);
                           }

                    }
                    if(!check){
                        lbl_con.setForeground(Color.red);
                        jList1.setCellRenderer(new DefaultListCellRenderer() {
                        @Override
                        public Component getListCellRendererComponent(JList list, Object value, int index,
                                   boolean isSelected, boolean cellHasFocus) {
                            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                            if(isSelected && list.getSelectedIndex()==3){
                               setBackground(Color.red);
                            }
                            return c;
                        }

                    });
                    }
                     
                    arr[i+1]=key;

                    jList1.setSelectedIndex(6);
                    sleep(delay);
                    panel.setXY(((i+1)*50)+75,20);
                    panel.setVisible(true);
                    lbl_key.setBackground(Color.green);
                    a[i+1].setBackground(Color.red);
                    sleep(delay);
                    a[i+1].setText(Integer.toString(key));
                    a[i+1].setBackground(Color.green);
                    sleep(delay); 
                    panel.setVisible(false);
                    System.out.println("The size of panel Height : "+panel.getHeight()+" width is " +panel.getWidth());
                    lbl_key.setBackground(Color.white);
                    a[i+1].setBackground(Color.white);
                } catch (InterruptedException ex) {
                    Logger.getLogger(InsertionSort.class.getName()).log(Level.SEVERE, null, ex);
                }
            
        }
               showMessage();
            }
        };
        
        M.start();    
    }
    
    public void showMessage(){
        JOptionPane.showMessageDialog(this,"Done ");
    
    }
    
    public void printArr(){
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
        
    }
    
    Font font =new Font("Tahoma",Font.BOLD,15);
    public void insertValues(){

        a = new JLabel[arr.length];
        index = new JLabel[arr.length];
        key.setBounds(110,100,30,30);
        key.setHorizontalAlignment(SwingConstants.CENTER);
        key.setVisible(false);
        lbl_key.setOpaque(true);
        key.setFont(font);
        ii.setBounds(70,130,30,30);
        ii.setVisible(false);
        ii.setHorizontalAlignment(SwingConstants.CENTER);
        ii.setFont(font);
        for(int i=0;i<arr.length;i++){
            a[i]=new JLabel(Integer.toString(arr[i]));
            index[i]=new JLabel(Integer.toString(i));
            a[i].setBounds(60+(i*50),120,30,30);
            index[i].setBounds(60+(i*50),80,30,30);
            a[i].setHorizontalAlignment(SwingConstants.CENTER);
            index[i].setHorizontalAlignment(SwingConstants.CENTER);
            a[i].setBackground(Color.yellow);
            a[i].setFont(font);
            index[i].setFont(font);
            a[i].setOpaque(true);
            jPanel2.add(a[i]);
            jPanel2.add(index[i]);
        }
        jPanel2.add(key);
        jPanel2.add(ii);
        if(arr.length>10){
            jPanel2.setPreferredSize(new Dimension(60*arr.length,jPanel2.getHeight()));
            panel.setBounds(0,170,60*arr.length,50);
        }
        jPanel2.repaint();
        jPanel3.repaint();
        panel.repaint();
        System.out.println("The size of the array is:"+arr.length);
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btn_start = new javax.swing.JButton();
        lbl_key2 = new javax.swing.JLabel();
        txt_values = new javax.swing.JTextField();
        btn_home = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        jSlider1 = new javax.swing.JSlider();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        lbl_key = new javax.swing.JLabel();
        lbl_key1 = new javax.swing.JLabel();
        lbl_keyIndex = new javax.swing.JLabel();
        lbl_key3 = new javax.swing.JLabel();
        lbl_iIndex = new javax.swing.JLabel();
        lbl_arrow = new javax.swing.JLabel();
        lbl_key5 = new javax.swing.JLabel();
        lbl_arrow2 = new javax.swing.JLabel();
        lbl_con = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        btn_start.setText("Simulate");
        btn_start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_startActionPerformed(evt);
            }
        });
        jPanel3.add(btn_start);
        btn_start.setBounds(500, 20, 100, 30);

        lbl_key2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_key2.setText("Values");
        jPanel3.add(lbl_key2);
        lbl_key2.setBounds(30, 90, 120, 30);
        jPanel3.add(txt_values);
        txt_values.setBounds(100, 90, 510, 30);

        btn_home.setText("Home");
        btn_home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_homeActionPerformed(evt);
            }
        });
        jPanel3.add(btn_home);
        btn_home.setBounds(20, 20, 110, 30);

        jPanel4.setBackground(new java.awt.Color(153, 153, 153));

        jList1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jScrollPane1.setViewportView(jList1);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Insertion Sort Algorithm");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );

        jSlider1.setMajorTickSpacing(500);
        jSlider1.setMaximum(3000);
        jSlider1.setMinimum(200);
        jSlider1.setMinorTickSpacing(100);
        jSlider1.setPaintTicks(true);
        jSlider1.setToolTipText("");
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        lbl_key.setBackground(new java.awt.Color(255, 255, 255));
        lbl_key.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_key.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_key.setOpaque(true);
        jPanel2.add(lbl_key);
        lbl_key.setBounds(130, 220, 50, 30);

        lbl_key1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_key1.setText("j Index");
        jPanel2.add(lbl_key1);
        lbl_key1.setBounds(360, 220, 80, 30);

        lbl_keyIndex.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_keyIndex.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel2.add(lbl_keyIndex);
        lbl_keyIndex.setBounds(420, 220, 50, 30);

        lbl_key3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_key3.setText(" i Index");
        jPanel2.add(lbl_key3);
        lbl_key3.setBounds(40, 260, 80, 30);

        lbl_iIndex.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_iIndex.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel2.add(lbl_iIndex);
        lbl_iIndex.setBounds(130, 260, 50, 30);
        jPanel2.add(lbl_arrow);
        lbl_arrow.setBounds(30, 100, 90, 50);

        lbl_key5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_key5.setText("Key Value");
        jPanel2.add(lbl_key5);
        lbl_key5.setBounds(40, 220, 80, 30);
        jPanel2.add(lbl_arrow2);
        lbl_arrow2.setBounds(250, 124, 90, 40);

        lbl_con.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jPanel2.add(lbl_con);
        lbl_con.setBounds(410, 10, 160, 30);

        jScrollPane2.setViewportView(jPanel2);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Step Delay");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 6, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, 556, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSlider1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_startActionPerformed
        
        if(txt_values.getText().isEmpty()){
            JOptionPane.showMessageDialog(this,"Insert values seperated by comma");
        }else{
            delay=jSlider1.getValue();
            boolean proceed=true;
            StringTokenizer st = new StringTokenizer(txt_values.getText(),",");
            arr = new int[st.countTokens()];
            for(int i=0;i<arr.length;i++){
               String token = st.nextToken();
                if(!isNumeric(token)){
                   JOptionPane.showMessageDialog(this,"Invalid input \""+token+"\"");
                   proceed=false;
                   break;
                }else{
                   arr[i]=Integer.parseInt(token);
                }
            }
            if(proceed){

                insertValues();
                insertionSort();
                printArr();
                btn_start.setEnabled(false);
            }
        }
    }//GEN-LAST:event_btn_startActionPerformed
    public boolean isNumeric(String text){
        
        for(int i=0;i<text.length();i++){
            if(!Character.isDigit(text.charAt(i))){
                return false;
            }
        }
        return true;
    }
        
    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
        
        JSlider source = (JSlider)evt.getSource();
        delay=source.getValue();
        
    }//GEN-LAST:event_jSlider1StateChanged

    private void btn_homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_homeActionPerformed
        Home h = new Home();
        h.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_homeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InsertionSort.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InsertionSort.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InsertionSort.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InsertionSort.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InsertionSort().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_home;
    private javax.swing.JButton btn_start;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JLabel lbl_arrow;
    private javax.swing.JLabel lbl_arrow2;
    private javax.swing.JLabel lbl_con;
    private javax.swing.JLabel lbl_iIndex;
    private javax.swing.JLabel lbl_key;
    private javax.swing.JLabel lbl_key1;
    private javax.swing.JLabel lbl_key2;
    private javax.swing.JLabel lbl_key3;
    private javax.swing.JLabel lbl_key5;
    private javax.swing.JLabel lbl_keyIndex;
    private javax.swing.JTextField txt_values;
    // End of variables declaration//GEN-END:variables
 

}
