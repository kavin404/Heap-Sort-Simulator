package daa;

import java.awt.*;
import javax.swing.*;

public class Bubble extends JPanel
{
    private int x;
    private int y;
    private Color c = Color.green;
    public Bubble()
   
    {
        super.setBackground(Color.white);
        super.setLayout(null);
    }
    public void setXY(int x,int y){
        this.x=x;
        this.y=y;
    }
    public void setRed(){
        c= Color.red;
    }
    public void setGreen(){
        c= Color.green;
    }
    public void setBlue(){
        c= Color.blue;
    }
    public void setMagenta(){
        c= Color.magenta;
    }
    @Override
    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(2));
       
            g.setColor(c);
            g.drawOval(x,y,30,30);  
       
    }
    public void draw(Color color){
        Graphics g = this.getGraphics();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(2));    
        g.setColor(color);
        g.drawOval(x,y,30,30);
    
    }
    

   
}