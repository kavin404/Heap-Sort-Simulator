
package daa;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import static java.lang.Thread.sleep;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

/**
 *
 * @author kavindu
 */
public class Heaps extends javax.swing.JFrame{

   
    int arr[];
    JLabel a[];
    JLabel m[];
    Bubble b[];
    
    boolean proceed=true;
    int heapSize=0;
   
    int delay=500;
    arrowPanel2 panel[];
    public Heaps() {
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int width=dim.width/2-(this.getWidth()/2);
        int height=  dim.height/2-(this.getHeight()/2);
        setBounds(width,height,this.getWidth(),this.getHeight());
        jPanel2.setPreferredSize(new Dimension(950,350));
        txt_values.requestFocus();
    }
    Thread M;
    public void showMessage(String text){
        JOptionPane.showMessageDialog(this,text);
    
    }
    
    public void printArr(){
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
    }
    
    
    public void getValues(){
        jPanel2.removeAll();
        jPanel2.repaint();
        proceed=true;
        if(txt_values.getText().isEmpty()){
            JOptionPane.showMessageDialog(this,"Error there are no values");
        }else{
            delay=jSlider1.getValue();
            StringTokenizer st = new StringTokenizer(txt_values.getText(),",");
            
            arr = new int[st.countTokens()];
            for(int i=0;i<arr.length;i++){
              String token = st.nextToken();
              if(isNumeric(token)){
                arr[i]=Integer.parseInt(token);
              }else{
                  JOptionPane.showMessageDialog(this,"Invalid value \""+token+"\"");
                  proceed=false;
                  break;
              }
            }
            if(proceed){
                insertValues();
                printArr();
            }
        }
       
    
    }
     public boolean isNumeric(String text){
        
        for(int i=0;i<text.length();i++){
            if(!Character.isDigit(text.charAt(i))){
                return false;
            }
        }
        return true;
    }
    Font font =new Font("Tahoma",Font.BOLD,15);
    public void insertValues(){
        int h=0;
        int totalElems = arr.length;
        int rowMaxElems=1;        
        int nextShift=0;
        int gap=0;
        int height = (int) Math.floor(Math.log10(totalElems+1)/Math.log10(2));
        int lastRow = (int) Math.pow(2,height);
        if(lastRow>16){
            jPanel2.setPreferredSize(new Dimension(lastRow*60,70*(height+1)));
        }
        a = new JLabel[arr.length];
        m = new JLabel[arr.length];
        b = new Bubble[arr.length];
        panel = new arrowPanel2[height+1];
        int start=0;
        int count=1;
        for(int i=0;i<totalElems;i++){
           
            a[i]=new JLabel(Integer.toString(arr[i]));
            m[i]=new JLabel(Integer.toString(i));
            b[i]=new Bubble();
            b[i].setXY(10,10);
            
            if(nextShift==i){
                 h++;
                 panel[h] = new arrowPanel2();
                 panel[h].setBounds(0,(h*60)+30,(int)jPanel2.getPreferredSize().getWidth(),40);
                 jPanel2.add(panel[h]);
                 nextShift=(int) (Math.pow(2,h+1)-1);
                 rowMaxElems=(int) Math.pow(2,h);
                 gap=(int)jPanel2.getPreferredSize().getWidth()/rowMaxElems;
                 start=gap/2;
                 panel[h].setXY(gap,rowMaxElems,h,start,arr.length-1,i+1);
                 count=1;
               
            }
           
            if(i==0){
                b[i].setBounds(((int)jPanel2.getPreferredSize().getWidth()/2)-25,50,50,50);
                m[i].setBounds(((int)jPanel2.getPreferredSize().getWidth()/2)-30,40,50,50);
            }else if(count==1){
                b[i].setBounds(start-25,((h+1)*60),50,50);
                m[i].setBounds(start-30,((h+1)*60)-10,50,50);
                count++;
            }else{
               
                b[i].setBounds(((count-1)*gap)+start-25,((h+1)*60),50,50);
                m[i].setBounds(((count-1)*gap)+start-30,((h+1)*60)-10,50,50);
                count++;
            }
            a[i].setHorizontalAlignment(SwingConstants.CENTER);
            a[i].setBackground(Color.white);
            a[i].setFont(font);
            a[i].setBounds(15,15,20,20);
            a[i].setOpaque(true);
            b[i].add(a[i]);
            jPanel2.add(b[i]);
            jPanel2.add(m[i]);
          
        }
      
        if(arr.length%2 ==0){
            panel[h].setBounds(0,(h*60)+30,b[(arr.length)/2-1].getX(),40);
        }else{
            panel[h].setBounds(0,(h*60)+30,b[(arr.length-1)].getX()+55,40);
        }
        jPanel2.repaint(); 
        jPanel2.revalidate();
        jPanel3.repaint();
        jPanel3.revalidate();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btn_showTree = new javax.swing.JButton();
        btn_refresh = new javax.swing.JButton();
        lbl_key2 = new javax.swing.JLabel();
        btn_heapSort = new javax.swing.JButton();
        btn_hipifyMax = new javax.swing.JButton();
        btn_hipifyMin = new javax.swing.JButton();
        txt_values = new javax.swing.JTextField();
        btn_heapSort1 = new javax.swing.JButton();
        btn_home = new javax.swing.JButton();
        jSlider1 = new javax.swing.JSlider();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        txt_operation = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        btn_showTree.setText("Show Tree");
        btn_showTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_showTreeActionPerformed(evt);
            }
        });
        jPanel3.add(btn_showTree);
        btn_showTree.setBounds(280, 20, 120, 30);

        btn_refresh.setText("Refresh");
        btn_refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_refreshActionPerformed(evt);
            }
        });
        jPanel3.add(btn_refresh);
        btn_refresh.setBounds(140, 20, 120, 30);

        lbl_key2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_key2.setText(" Values");
        jPanel3.add(lbl_key2);
        lbl_key2.setBounds(30, 70, 100, 30);

        btn_heapSort.setText("Heap Sort (acending)");
        btn_heapSort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_heapSortActionPerformed(evt);
            }
        });
        jPanel3.add(btn_heapSort);
        btn_heapSort.setBounds(700, 20, 135, 30);

        btn_hipifyMax.setText("Build Max Heap");
        btn_hipifyMax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hipifyMaxActionPerformed(evt);
            }
        });
        jPanel3.add(btn_hipifyMax);
        btn_hipifyMax.setBounds(410, 20, 120, 30);

        btn_hipifyMin.setText("Build Min Heap");
        btn_hipifyMin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hipifyMinActionPerformed(evt);
            }
        });
        jPanel3.add(btn_hipifyMin);
        btn_hipifyMin.setBounds(550, 20, 130, 30);
        jPanel3.add(txt_values);
        txt_values.setBounds(110, 70, 880, 30);

        btn_heapSort1.setText("Heap Sort (decending)");
        btn_heapSort1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_heapSort1ActionPerformed(evt);
            }
        });
        jPanel3.add(btn_heapSort1);
        btn_heapSort1.setBounds(850, 20, 141, 30);

        btn_home.setText("Home");
        btn_home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_homeActionPerformed(evt);
            }
        });
        jPanel3.add(btn_home);
        btn_home.setBounds(10, 20, 110, 30);

        jSlider1.setBackground(new java.awt.Color(0, 102, 102));
        jSlider1.setMajorTickSpacing(500);
        jSlider1.setMaximum(3000);
        jSlider1.setMinimum(200);
        jSlider1.setMinorTickSpacing(100);
        jSlider1.setPaintTicks(true);
        jSlider1.setToolTipText("");
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);
        jScrollPane1.setViewportView(jPanel2);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        txt_operation.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(txt_operation, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txt_operation, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSlider1, javax.swing.GroupLayout.DEFAULT_SIZE, 999, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_showTreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_showTreeActionPerformed
        getValues();
        
    }//GEN-LAST:event_btn_showTreeActionPerformed

    private void btn_refreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_refreshActionPerformed
        txt_values.setText(null);
        txt_operation.setText("");
        arr=null;
        jPanel2.removeAll();
        jPanel2.repaint();
        jPanel2.revalidate();
        jPanel2.setPreferredSize(new Dimension(950,350));
    }//GEN-LAST:event_btn_refreshActionPerformed
    
    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
        
        JSlider source = (JSlider)evt.getSource();
        delay=source.getValue();
        
    }//GEN-LAST:event_jSlider1StateChanged

    private void btn_heapSortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_heapSortActionPerformed
       
        getValues();
        if(proceed){
            heap_sort_acending();
            printArr();
            txt_operation.setText("Heap Sort (Acending Order)");
        }
    
    }//GEN-LAST:event_btn_heapSortActionPerformed

    private void btn_hipifyMaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hipifyMaxActionPerformed
       
        getValues();
        if(proceed){
            build_heap_max();
            printArr();
            txt_operation.setText("Build Max Heap");
        }
    }//GEN-LAST:event_btn_hipifyMaxActionPerformed
    public void heap_sort_acending(){
        heapSize= arr.length;
        Thread M = new Thread(){
            public void run(){
                for(int i=(arr.length/2)-1;i>=0;i--){
                    max_hipify(arr,i);
                }
                System.out.println("after max hipify");
                printArr();
                for(int i=arr.length-1;i>0;i--){
                    int temp= arr[0];
                    arr[0]=arr[i];
                    
                    a[0].setText(a[i].getText());
                    arr[i]=temp;
                    a[i].setText(Integer.toString(temp));
                   
                    b[i].draw(Color.red);
                    b[i].setRed();
                    printArr();
                    heapSize = heapSize-1;
                    max_hipify(arr,0);

                    printArr();
                }
                
                String text ="Finished sorting result is\n";
                for(int i=0;i<arr.length;i++){
                    text = text+" , "+arr[i];
                }
                                
                showMessage(text);
            }
        };
        M.start();
    }
    public void heap_sort_decending(){
        heapSize= arr.length;
        Thread M = new Thread(){
            public void run(){
                for(int i=(arr.length/2)-1;i>=0;i--){
                    min_hipify(arr,i);
                }
               
                printArr();
                for(int i=arr.length-1;i>0;i--){
                    int temp= arr[0];
                    arr[0]=arr[i];
                    
                    a[0].setText(a[i].getText());
                    arr[i]=temp;
                    a[i].setText(Integer.toString(temp));
                    b[i].draw(Color.red);
                    b[i].setRed();
                    printArr();
                    heapSize = heapSize-1;
                    min_hipify(arr,0);

                    printArr();
                }
                String text ="Finished sorting result is\n";
                for(int i=0;i<arr.length;i++){
                    text = text+" , "+arr[i];
                }
                                
                showMessage(text);
            }
            
        };
        M.start();
    }
    
    public void build_heap_max(){
        heapSize=arr.length;
        Thread M = new Thread(){
            public void run(){
                for(int i=(arr.length/2)-1;i>=0;i--){                      
                       max_hipify(arr,i);
                }
            }
        };
        M.start();
    }
    
    public void build_heap_min(){
        heapSize=arr.length;
        Thread M = new Thread(){
            public void run(){
                for(int i=(arr.length/2)-1;i>=0;i--){
                      
                       min_hipify(arr,i);
                }
            }
        };
        M.start();
    }
    public void max_hipify(int arr[],int i){
                int l = LEFT_CHILD(i);
                int r = RIGHT_CHILD(i);
                 
                int largest;
                if(l<=heapSize-1 && arr[l]>arr[i]){
                    largest = l;
        
                }else{
                    largest =i;
                }
        
                if(r<=heapSize-1 && arr[r]>arr[largest]){
                    largest= r;
                }
                if(largest != i){
                    
                        int temp= arr[i];
                        arr[i]=arr[largest];
                        b[i].setMagenta();
                        b[i].draw(Color.MAGENTA);
                        b[largest].setBlue();
                        b[largest].draw(Color.blue);
                        
                    try {
                        sleep(2000);
                       
                        b[i].setMagenta();
                        b[largest].setBlue();
                        b[i].draw(Color.blue);
                        b[largest].draw(Color.MAGENTA);
                        sleep(1000);
                        a[i].setText(Integer.toString(arr[largest]));
                        a[largest].setText(Integer.toString(temp));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Heaps.class.getName()).log(Level.SEVERE, null, ex);
                        
                    }
                        b[i].setGreen();
                        b[largest].setGreen();
                        b[i].draw(Color.green);
                        b[largest].draw(Color.green);
                        arr[largest]=temp;
                        max_hipify(arr,largest);
                    
                }
    }
    
       public void min_hipify(int arr[],int i){
                int l = LEFT_CHILD(i);
                int r = RIGHT_CHILD(i);
             
                int smallest;
                if(l<= heapSize-1 && arr[l]<arr[i]){
                    smallest = l;
        
                }else{
                    smallest =i;
                }
        
                if(r<=heapSize-1 && arr[r]<arr[smallest]){
                    smallest= r;
                }
                if(smallest != i){
                    
                        int temp= arr[i];
                        arr[i]=arr[smallest];
                           b[i].setMagenta();
                        b[i].draw(Color.MAGENTA);
                        b[smallest].setBlue();
                        b[smallest].draw(Color.blue);
                        
                    try {
                        sleep(2000);
                        
                        b[i].setMagenta();
                        b[smallest].setBlue();
                        b[i].draw(Color.blue);
                        b[smallest].draw(Color.MAGENTA);
                        sleep(1000);
                        a[i].setText(Integer.toString(arr[smallest]));
                        a[smallest].setText(Integer.toString(temp));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Heaps.class.getName()).log(Level.SEVERE, null, ex);
                        
                    }
                        b[i].setGreen();
                        b[smallest].setGreen();
                        b[i].draw(Color.green);
                        b[smallest].draw(Color.green);
                        arr[smallest]=temp;
                        min_hipify(arr,smallest);
                    
                }
    }
    public static int LEFT_CHILD(int i){
        if(i==0){
            return 1;
        }
        return(2*i)+1;
    }
    
    public static int RIGHT_CHILD(int i){
        if(i==0){
            return 2;
        }
        return (2*i)+2;
    }
    private void btn_hipifyMinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hipifyMinActionPerformed
        
        getValues();
        if(proceed){
            build_heap_min();
            txt_operation.setText("Build Min Heap");
        }
    }//GEN-LAST:event_btn_hipifyMinActionPerformed

    private void btn_heapSort1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_heapSort1ActionPerformed
        
        getValues();
        if(proceed){
            heap_sort_decending();
            txt_operation.setText("Heap Sort (Decending Order)");
        }
       
    }//GEN-LAST:event_btn_heapSort1ActionPerformed

    private void btn_homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_homeActionPerformed
        Home h= new Home();
        h.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_homeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Heaps.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Heaps.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Heaps.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Heaps.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Heaps().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_heapSort;
    private javax.swing.JButton btn_heapSort1;
    private javax.swing.JButton btn_hipifyMax;
    private javax.swing.JButton btn_hipifyMin;
    private javax.swing.JButton btn_home;
    private javax.swing.JButton btn_refresh;
    private javax.swing.JButton btn_showTree;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JLabel lbl_key2;
    private javax.swing.JLabel txt_operation;
    private javax.swing.JTextField txt_values;
    // End of variables declaration//GEN-END:variables

    
 

}
